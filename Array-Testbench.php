<? 
// Array Testbench for testing Array functions
 

// phonebook array test

// the Code below isn't going to work because $john, $bill, $sarah is not defined
/*
$phonebook = array(
	$john = array('first_name' => "john", 'last_name' => "Smith", 'Number' => "555-0162",);
	$Bill = array('first_name' => "bill", 'last_name' => "Wilson", 'Number' => "555-2483",);
	$Sarah = array('first_name' => "sarah", 'last_name' => "Wu", 'Number' => "555-8832",);
	);
*/
//foreach ()

// This is the proper way to load the array

$phonebook = array(
	'john-smith' => array(
			'first_name' => 'John',
			'last_name' => 'Smith',
			'number' => '123-1234',
	),
	
	'bill-wilson' => array(
			'first_name' => 'Bill',
			'last_name' => 'Wilson',
			'number' => '412-1233',	
	),

	'sarah-wu' => array(
			'first_name' => 'Sarah',
			'last_name' => 'Wu',
			'number' => '412-1231',	
	),

);

var_dump($phonebook);

// here's how you iterate through the associative array data structure:
foreach( $phonebook as $key => $val ) {
	printf("(%s) %s %s - %s", $key, $val['first_name'], $val['last_name'], $val['number']);
}


