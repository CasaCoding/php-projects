
<?
// Vending Machine php project

//	Set scale for BCmath
	bcscale(2);
// calculate random sales value between 0 and 15 dollars
	$total = rand(1,2000);
	$total = (float)$total;
	$total_out = (float)bcdiv($total , 100.00, 2);
	printf("\r\n", $total);
	

// Output sales cost statement and ask how the customer would like to pay and save customer input
	printf("Your total due is $%.2f.\r\n", $total_out);

// Get Change and verify data input
	printf("How would you like to pay?\r\n");
	$payment_in = fgets(STDIN);
	$payment = (float)$payment_in;
	$payment = bcmul($payment, 100); 										//Convert decimal dollars to cents for calculations
	
	$change_due = (float)(bcsub($payment , $total));
	
//	if($change_due > 0) {
		$change = (float)bcdiv($change_due, 100, 2);						//Convert Change to integer for calculations
		printf("Here is your change.\r\n $%.2f\r\n", $change);
		
//	Calculate quantity of each bill type
		$hundreds = floor(bcdiv($change_due , 10000));
		$change_due = bcsub($change_due , (bcmul($hundreds, 10000)));
		
		$fifties = floor(bcdiv($change_due , 5000));
		$change_due = bcsub($change_due , (bcmul($fifties, 5000)));

		$twenties = floor(bcdiv($change_due , 2000));
		$change_due = bcsub($change_due , (bcmul($twenties, 2000)));

		$tens = floor(bcdiv($change_due , 1000));
		$change_due = bcsub($change_due , (bcmul($tens, 1000)));

		$fives = floor(bcdiv($change_due , 500));
		$change_due = bcsub($change_due , (bcmul($fives, 500)));

		$ones = floor(bcdiv($change_due , 100));
		$change_due = bcsub($change_due , bcmul($ones , 100));

		$quarters = floor(bcdiv($change_due , 25));
		$change_due = bcsub($change_due , (bcmul($quarters, 25)));

		$dimes = floor(bcdiv($change_due , 10));
		$change_due = bcsub($change_due , (bcmul($dimes, 10)));

		$nickels = floor(bcdiv($change_due , 5));
		$change_due = bcsub($change_due , (bcmul($nickels, 5)));

		$pennies = $change_due;
		$change_due = bcsub($change_due , $pennies);

// Function to determind which bills to output
		
		// print change based on change amount
		// change if > 100
/*		if ($change >=10000) {
*/					printf("Hudreds = %d\r\n", $hundreds);
					printf("Fifties = %d\r\n", $fifties);
					printf("Twenties = %d\r\n", $twenties);
					printf("Tens = %d\r\n", $tens);
					printf("Fives = %d\r\n", $fives);
					printf("Ones = %d\r\n", $ones);
					printf("Quarters = %d\r\n", $quarters);
					printf("Dimes = %d\r\n", $dimes);
					printf("Nickles = %d\r\n", $nickels);
					printf("Pennies = %d\r\n", $pennies);			
/*		}

		// change if > 50
		else if ($change >= 5000) {
					printf("Fifties = %d\r\n", $fifties);
					printf("Twenties = %d\r\n", $twenties);
					printf("Tens = %d\r\n", $tens);
					printf("Fives = %d\r\n", $fives);
					printf("Ones = %d\r\n", $ones);
					printf("Quarters = %d\r\n", $quarters);
					printf("Dimes = %d\r\n", $dimes);
					printf("Nickles = %d\r\n", $nickels);
					printf("Pennies = %d\r\n", $pennies);			
		} 

		// change if > 20
		else if ($change >= 2000) {
					printf("Twenties = %d\r\n", $twenties);
					printf("Tens = %d\r\n", $tens);
					printf("Fives = %d\r\n", $fives);
					printf("Ones = %d\r\n", $ones);
					printf("Quarters = %d\r\n", $quarters);
					printf("Dimes = %d\r\n", $dimes);
					printf("Nickles = %d\r\n", $nickels);
					printf("Pennies = %d\r\n", $pennies);				
		} 

		// change if > 10
		else if ($change >= 1000) {
					printf("Tens = %d\r\n", $tens);
					printf("Fives = %d\r\n", $fives);
					printf("Ones = %d\r\n", $ones);
					printf("Quarters = %d\r\n", $quarters);
					printf("Dimes = %d\r\n", $dimes);
					printf("Nickles = %d\r\n", $nickels);
					printf("Pennies = %d\r\n", $pennies);			
		} 

		// change in fives
		else if ($change >= 500) {							
					printf("Fives = %d\r\n", $fives);
					printf("Ones = %d\r\n", $ones);
					printf("Quarters = %d\r\n", $quarters);
					printf("Dimes = %d\r\n", $dimes);
					printf("Nickles = %d\r\n", $nickels);
					printf("Pennies = %d\r\n", $pennies);		
		}

		// change in ones
		else if ($change >= 100) {							
					printf("Ones = %d\r\n", $ones);
					printf("Quarters = %d\r\n", $quarters);
					printf("Dimes = %d\r\n", $dimes);
					printf("Nickles = %d\r\n", $nickels);
					printf("Pennies = %d\r\n", $pennies);			
		}

		// change in quarters
		else if ($change >= 25) {							
					printf("Quarters = %d\r\n", $quarters);
					printf("Dimes = %d\r\n", $dimes);
					printf("Nickles = %d\r\n", $nickels);
					printf("Pennies = %d\r\n", $pennies);			
		}

		// change in dimes
		else if ($change >= 10) {							
					printf("Dimes = %d\r\n", $dimes);
					printf("Nickles = %d\r\n", $nickels);
					printf("Pennies = %d\r\n", $pennies);
		}

		// change in nickles
		else if ($change >= 5) {							
					printf("Nickles = %d\r\n", $nickels);
					printf("Pennies = %d\r\n", $pennies);
		}

		// change in pennies
		else if ($change > 0) {							
			printf("Pennies = %d\r\n", $pennies);
		} Else {
		printf("Exact Change!!\r\n");	
	}   */
printf("We now owe you $%.2f.\r\nThank you for coming! Have a wonderful day!\r\n", $change_due);
/*
printf("\r\n\r\n");

// Variable Value Test
printf("rand \r\n");
var_dump($rand);
printf("\r\n");
printf("total\r\n");
var_dump($total);
printf("\r\n");
printf("fpayment\r\n");
var_dump($fpayment);
printf("payment\r\n");
var_dump($payment);
printf("\r\n");
printf("change\r\n");
var_dump($change);
printf("\r\n");
printf("change_due\r\n");
var_dump($change_due);
printf("\r\n");
printf("hundreds\r\n");
var_dump($hundreds);
printf("\r\n");
var_dump($twenties);
printf("\r\n");
var_dump($tens);
printf("\r\n");
var_dump($fives);
printf("\r\n");
var_dump($ones);
printf("\r\n");
var_dump($quarters);
printf("\r\n");
var_dump($dimes);
printf("\r\n");
var_dump($nickels);
printf("\r\n");
var_dump($pennies);
printf("\r\n");
*/
?>





