<?  //Palindrome Test Software

//	Initialize do-while loop
	do {

//	Word input
		printf("Enter a Word \r\n");
		$f = fgets(STDIN);

//remove spaces
		$f = str_replace("  ","", $f);

//remove any special characters
		$f = preg_replace('/[^A-Za-z0-9\-]/','', $f);
/* preg means php regular expression
	replace /[^A-Za-z0-9\-]/ expressions
		forward slash end forward slash = anything in between these means look for the pattern in between them
		left open braket and closed open braket = look for a range of characters (when you put something in brakets mean you look for a range of something)
		^ = boolean operator meaning !not ... aka find all the characters !not in this range
			A-Z = first range aka look for letters that are not capitol A to Capitol Z and don't delete it
			a-z = second range and look for these and don't delete
			0-9 = third range and also don't delete
		\- means one or more (if I encounter a sequence of charachters not these ranges delete them) 
			]/ closing open braket and closing forward slash

	*/
//Make string all one case 
		$f = strtolower($f);

// Reverse String
		$revf = strrev($f);

//	Check if string & reverse string values match
		
		if ($f == $revf) { 
			printf("This is a palindrome!!\r\n");
		} Else {
			printf("This is not a palindrome \r\n");
		}


?>